package com.example.trafficlight;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    LinearLayout LinearLayout;
    Button btn_red;
    Button btn_yellow;
    Button btn_green;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayout = findViewById(R.id.relativeLayout);
        btn_red = findViewById(R.id.btn_red);
        btn_yellow = findViewById(R.id.btn_yellow);
        btn_green = findViewById(R.id.btn_green);
    }

    public void onClickBtnRed(View view){
        LinearLayout.setBackgroundColor(getResources().getColor(R.color.colorRed, null));
    }

    public void onClickBtnYellow(View view){
        LinearLayout.setBackgroundColor(getResources().getColor(R.color.colorYellow, null));
    }

    public void onClickBtnGreen(View view){
        LinearLayout.setBackgroundColor(getResources().getColor(R.color.colorGreen, null));
    }
}